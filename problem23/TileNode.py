from enum import Enum


class TileType(Enum):
    t = "wall"
    f = "free"


class TileNode:
    x: int
    y: int
    neighbours: ['TileNode']
    tile_type: TileType

    def __init__(self, x: int, y: int, tile_type_raw: str):
        self.tile_type = TileType[tile_type_raw]
        self.y = y
        self.x = x
        self.neighbours = []

    def bind_neighbours(self, board: {(int, int): 'TileNode'}):
        search_range = [(-1, 0), (0, -1), (1, 0), (0, 1)]
        for neighbour_location in search_range:
            x = self.x - neighbour_location[0]
            y = self.y - neighbour_location[1]
            try:
                neighbour = board[(x, y)]
            except KeyError:
                continue

            if neighbour.tile_type == TileType.t:
                continue

            self.neighbours.append(neighbour)

    def __str__(self):
        return "{},{} {}".format(self.x, self.y, self.tile_type)
