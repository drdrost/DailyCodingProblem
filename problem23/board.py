from problem23.TileNode import TileNode


class Board:
    board: {(int, int): TileNode}

    def __init__(self, raw_board: str):
        self.board = self.read_board(raw_board)
        self.bind_neighbours()

    def shortest_route(self, start_location: tuple, end_location: tuple) -> int:
        start_node = self.board[start_location]
        target_node = self.board[end_location]
        visited = [start_node]

        nodes = [start_node]
        distances = {node: None for node in self.board.values()}
        distances[start_node] = 0

        while nodes:
            current_node = min(nodes, key=lambda node: distances[node])
            nodes.remove(current_node)
            current_distance = distances[current_node]

            for neighbour in [x for x in current_node.neighbours if x not in visited]:
                nodes.append(neighbour)
                distances[neighbour] = current_distance + 1
                visited.append(neighbour)

                if neighbour == target_node:
                    break

        return distances[target_node]

    @staticmethod
    def read_board(raw_board: str) -> {(int, int): TileNode}:
        raw_board = raw_board.replace("f", "\"f\"")
        raw_board = raw_board.replace("t", "\"t\"")

        board = {}
        x = 0
        for raw_row in eval(raw_board):
            y = 0
            for raw_tile in raw_row:
                board[x, y] = (TileNode(x, y, raw_tile))
                y += 1
            x += 1

        return board

    def get_tile(self, location: tuple) -> TileNode:
        x = location[0]
        y = location[1]
        return self.board[x, y]

    def bind_neighbours(self):
        for location, tile in self.board.items():
            tile.bind_neighbours(self.board)
