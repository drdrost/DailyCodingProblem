"""
You are given an M by N matrix consisting of booleans that represents a board. Each True boolean represents a wall.
Each False boolean represents a tile you can walk on.

Given this matrix, a start coordinate, and an end coordinate,
return the minimum number of steps required to reach the end coordinate from the start.
If there is no possible path, then return null. You can move up, left, down, and right. You cannot move through walls.
You cannot wrap around the edges of the board.

For example, given the following board:

[[f, f, f, f],
[t, t, f, t],
[f, f, f, f],
[f, f, f, f]]
and start = (3, 0) (bottom left) and end = (0, 0) (top left),
the minimum number of steps required to reach the end is 7,
since we would need to go through (1, 2) because there is a wall everywhere else on the second row.
"""
import pytest

from problem23.board import Board
from problem23.TileNode import TileType

test_board1 = """[[f, f, f, f],
                 [t, t, f, t],
                 [f, f, f, f],
                 [f, f, f, f]]"""


@pytest.mark.parametrize("raw_board, test_locations_tile_types", [
    (test_board1, [
        ((0, 0), TileType.f),
        ((3, 0), TileType.f),
        ((0, 3), TileType.f),
        ((1, 3), TileType.t),
        ((1, 1), TileType.t),
        ((1, 2), TileType.f),
    ])
])
def test_read_board(raw_board: str, test_locations_tile_types: [(tuple, TileType)]):
    board = Board(raw_board)
    for test_location_tile_type in test_locations_tile_types:
        assert board.get_tile(test_location_tile_type[0]).tile_type == test_location_tile_type[1]


@pytest.mark.parametrize("raw_board, test_locations_neighbours", [
    (test_board1, [
        ((0, 0), (0, 1)),
        ((3, 0), (3, 1)),
        ((3, 0), (2, 0)),
        ((2, 2), (2, 1)),
        ((2, 2), (2, 3)),
        ((2, 2), (1, 2)),
        ((2, 2), (3, 2))
    ])
])
def test_neighbours(raw_board: str, test_locations_neighbours: [(int, int), (int, int)]):
    board = Board(raw_board)
    for test_locations_neighbour in test_locations_neighbours:
        neighbour = board.get_tile(test_locations_neighbour[1])
        assert neighbour in board.get_tile(test_locations_neighbour[0]).neighbours


@pytest.mark.parametrize("raw_board, test_locations_neighbours", [
    (test_board1, [
        ((0, 0), (1, 0)),
        ((3, 0), (2, 1)),
        ((3, 0), (2, 3)),
        ((2, 2), (1, 1)),
        ((2, 2), (3, 3)),
        ((2, 2), (1, 1))
    ])
])
def test_not_neighbours(raw_board: str, test_locations_neighbours: [(int, int), (int, int)]):
    board = Board(raw_board)
    for test_locations_neighbour in test_locations_neighbours:
        neighbour = board.get_tile(test_locations_neighbour[1])
        assert neighbour not in board.get_tile(test_locations_neighbour[0]).neighbours


@pytest.mark.parametrize("raw_board, start_location, end_location, minimum_steps", {
    (test_board1, (3, 0), (0, 0), 7),
    (test_board1, (3, 0), (3, 0), 0),
    (test_board1, (0, 3), (0, 0), 3),
})
def test_shortest_route(raw_board: str, start_location: tuple, end_location: tuple, minimum_steps: int):
    board = Board(raw_board)
    assert minimum_steps == board.shortest_route(start_location, end_location)
