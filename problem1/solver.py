import pytest


def solver(input_list: [int], input_number: int) -> bool:
    new_list = [input_number - x for x in input_list]
    return any(i in new_list for i in input_list)


@pytest.mark.parametrize("input_list, input_number, expected", [
    ([10, 15, 3, 7], 17, True),
    ([10, 15, 3, 7], 50, False)
])
def test_solver_true(input_list, input_number, expected):
    assert expected == solver(input_list, input_number)

