"""
Implement locking in a binary tree.
A binary tree node can be locked or unlocked only if all of its descendants or ancestors are not locked.

Design a binary tree node class with the following methods:

is_locked, which returns whether the node is locked
lock, which attempts to lock the node. If it cannot be locked, then it should return false.
Otherwise, it should lock it and return true.
unlock, which unlocks the node. If it cannot be unlocked, then it should return false. Otherwise,
it should unlock it and return true.
You may augment the node to add parent pointers or any other property you would like.
You may assume the class is used in a single-threaded program, so there is no need for actual locks or mutexes.
Each method should run in O(h), where h is the height of the tree.
"""
import pytest

from problem24.tree_node import TreeNode


@pytest.mark.parametrize("to_lock", [
    ({1: True}),
    ({2: True}),
    ({222: True, 2: False}),
    ({2: True, 222: False, 221: False}),
    ({222: True, 221: True, 1: True, 0: False, 12: False}),
])
def test_lock(to_lock: {int: bool}):
    top_node = TreeNode("top")
    left1 = TreeNode("1")
    right2 = TreeNode("2")
    left11 = TreeNode("11")
    right12 = TreeNode("12")
    right22 = TreeNode("22")
    left221 = TreeNode("221")
    right222 = TreeNode("222")

    top_node.add_children([left1, right2])
    left1.add_children([left11, right12])
    right2.add_children([right22])
    right22.add_children([left221, right222])

    tree = {
        0: top_node,
        1: left1,
        2: right2,
        11: left11,
        12: right12,
        22: right22,
        221: left221,
        222: right222
    }

    for tree_node_number, possible in to_lock.items():
        node = tree[tree_node_number]
        assert possible == node.lock()
        assert possible == node.is_locked
