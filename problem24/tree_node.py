class TreeNode:
    is_locked: bool
    children: ['TreeNode']
    parent: 'TreeNode'
    name: str

    def __init__(self, name: str, parent: 'TreeNode' = None):
        self.name = name
        self.is_locked = False
        self.children = []
        self.parent = parent

    def lock(self):
        can_change_lock = self.can_change_lock()
        if self.is_locked or not can_change_lock:
            return False

        self.is_locked = True
        return True

    def unlock(self):
        if not self.is_locked or not self.can_change_lock():
            return False

        self.is_locked = False
        return True

    def add_child(self, child: 'TreeNode'):
        if len(self.children) > 1:
            raise ValueError("there can not be more than 2 children in an binary tree")

        self.children.append(child)
        child.parent = self

    def add_children(self, children: ['TreeNode']):
        for child in children:
            self.add_child(child)

    def can_change_lock(self) -> bool:
        if self.can_change_lock_child() and self.can_change_lock_parent():
            return True
        return False

    def can_change_lock_parent(self) -> bool:
        if self.is_locked:
            return False

        if self.parent is not None:
            return self.parent.can_change_lock_parent()

        return True

    def can_change_lock_child(self) -> bool:
        if self.is_locked:
            return False

        for child in self.children:
            if not child.can_change_lock_child():
                return False

        return True

    def __str__(self):
        return self.name
