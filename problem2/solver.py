from functools import reduce

import pytest


def productizer(input_array):
    place = 0
    for number in input_array:
        new_list = input_array.copy()
        del (new_list[place])

        product = reduce((lambda x, y: x * y), new_list)
        place += 1

        yield product


@pytest.mark.parametrize("input_array, expected", [
    ([1, 2, 3, 4, 5], [120, 60, 40, 30, 24]),
    ([3, 2, 1], [2, 3, 6]),

])
def test_solver(input_array, expected):
    assert expected == list(productizer(input_array))
