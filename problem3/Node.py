import pickle


class Node:
    name: str
    left: "Node"
    right: "Node"

    def __init__(self, name: str, left: "Node" = None, right: "Node" = None) -> None:
        self.name = name
        self.left = left
        self.right = right

    def serialize(self) -> bytes:
        return serialize(self)

    def __eq__(self, other):
        return self.__dict__ == other.__dict__


def serialize(node: Node) -> bytes:
    return pickle.dumps(node)


def deserialize(node_string: bytes) -> Node:
    return pickle.loads(node_string)
