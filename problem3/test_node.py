import pytest

from problem3.Node import Node, deserialize, serialize


@pytest.mark.parametrize("node, expected", [
    (Node('root'), b'\x80\x03cproblem3.Node\nNode\nq\x00)\x81q\x01}q\x02('
                   b'X\x04\x00\x00\x00nameq\x03X\x04\x00\x00\x00rootq\x04X\x04\x00\x00\x00leftq\x05NX\x05\x00\x00'
                   b'\x00rightq\x06Nub.'),
    (Node('root', Node("left")), b'\x80\x03cproblem3.Node\nNode\nq\x00)\x81q\x01}q\x02('
                                 b'X\x04\x00\x00\x00nameq\x03X\x04\x00\x00\x00rootq\x04X\x04\x00\x00\x00leftq\x05h'
                                 b'\x00)\x81q\x06}q\x07(h\x03h\x05h\x05NX\x05\x00\x00\x00rightq\x08Nubh\x08Nub.'),
])
def test_serialize(node: Node, expected: bytes):
    serialized = serialize(node)
    assert expected == serialized


@pytest.mark.parametrize("node_string, expected", [
    (b'\x80\x03cproblem3.Node\nNode\nq\x00)\x81q\x01}q\x02('
     b'X\x04\x00\x00\x00nameq\x03X\x04\x00\x00\x00rootq\x04X\x04\x00\x00\x00leftq\x05NX\x05\x00\x00'
     b'\x00rightq\x06Nub.',
     Node('root')),
    (b'\x80\x03cproblem3.Node\nNode\nq\x00)\x81q\x01}q\x02('
     b'X\x04\x00\x00\x00nameq\x03X\x04\x00\x00\x00rootq\x04X\x04\x00\x00\x00leftq\x05h'
     b'\x00)\x81q\x06}q\x07(h\x03h\x05h\x05NX\x05\x00\x00\x00rightq\x08Nubh\x08Nub.',
     Node('root', Node("left")))
])
def test_deserialize(node_string: bytes, expected: Node):
    assert expected == deserialize(node_string)


@pytest.mark.parametrize("tree, expected", [
    (Node('root', Node('left', Node('left.left')), Node('right')), 'left.left'),
])
def test_total(tree: Node, expected: str):
    node = tree
    assert deserialize(serialize(node)).left.left.name == expected
