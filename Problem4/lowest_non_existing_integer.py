"""
This problem was asked by Stripe.

Given an array of integers, find the first missing positive integer in linear time and constant space.
In other words, find the lowest positive integer that does not exist in the array.
The array can contain duplicates and negative numbers as well.

For example, the input [3, 4, -1, 1] should give 2. The input [1, 2, 0] should give 3.

You can modify the input array in-place.
"""

import pytest


def lowest_non_existing_integer_loop(input_list: [int]) -> int:
    for x in range(1, len(input_list) + 2):
        if x not in input_list:
            return x
    raise Exception("No solution found")


def lowest_non_existing_integer_set(input_list: [int]) -> int:
    return min(set(range(1, len(input_list) + 2)) - set(input_list))


test_data = [
    ([10, 15, 3, 7], 1),
    ([3, 4, -1, 1], 2),
    ([1, 2, 0], 3),
    ([1, 2, 0, -4, -4, 3], 4),
    ([1, 2, 3, 4], 5),
    ([-1, -2, -3, -4], 1)
]


@pytest.mark.parametrize("input_list, expected", test_data)
def test_lowest_number_loop(input_list: [int], expected: int):
    assert expected == lowest_non_existing_integer_loop(input_list)


@pytest.mark.parametrize("input_list, expected", test_data)
def test_lowest_number_set(input_list: [int], expected: int):
    assert expected == lowest_non_existing_integer_set(input_list)
