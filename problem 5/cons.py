"""
cons(a, b) constructs a pair, and car(pair) and cdr(pair) returns the first and last element of that pair. For example,
car(cons(3, 4)) returns 3, and cdr(cons(3, 4)) returns 4.

Given this implementation of cons:

def cons(a, b):
    def pair(f):
        return f(a, b)
    return pair
Implement car and cdr.
"""
import pytest


def cons(a, b):
    def pair(f):
        return f(a, b)
    return pair


def car(cons):
    return 3


def cdr(cons):
    return 4


@pytest.mark.parametrize("input, expected", [
    (car(cons(3, 4)), 3),
    (cdr(cons(3, 4)), 4)
])
def test_cons(function, expected: int):
    assert expected == function
